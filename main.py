from signal_processing import file_read


'''
Table to choose exp_num :
0 : Sans Stim Sans Filtre
1 : 1127
2 : 1128
3 : Avec Stim Sans Filtre
4 : Sans Stim Avec Filtre
5 : Avec Stim Avec Filtre
6 : Sans Stimulation Filtrerefcommun
#6bis : Avec Stimulation Filtrerefcommun Marche pas, pas de header
7 : Expérience sur place #1 (voir condition sur excel) 
8 : Expérience sur place #2 (//)
9 : Expérience sur place #3 
10: Expérience sur place #4 (big fibers)
'''

file_read.load_exp (exp_num=9, 
                synchronisation=False,
                single_experiment=False,
                time_num=1,
                data_degradation=False,
                degradation_num=10,
                degradation_index=9,
                band=False,
                low=False, 
                order=3,
                notch=False, 
                fmin=0,
                fmax=100,
                Q=1, 
                shows=True,
                barre_a_delta=True,
                barre_b=False,
                barre_c=False,
                barre_grosse_fibre=True,
                trims=True,
                timeframe=[2.5,210], 
                detrend=True,
                spectre=True,
                correlation=True,
                out=False,
                sg=True,
                Nfft=64,
                interpolation=False)


'''
Notes :
Put here the setups that corresponds to each fiber 

# BIG FIBERS

file_read.load_exp(exp_num=10, 
                band=False,
                low=True, #True : only fmax is needed 
                order=2,
                notch=False, 
                fmin=0.1,
                fmax=500, 
                Q=1, 
                shows=True, 
                trims=True,
                timeframe=[1.3,3], 
                detrend=True,
                spectre=True,
                cor=True,
                out=False,
                sg=True) 

# ALTERNATE VIEW OF BIG FIBERS
file_read.load_exp(exp_num=10, 
                band=False,
                low=True, #True : only fmax is needed 
                order=1,
                notch=False, 
                fmin=0.1,
                fmax=900, 
                Q=1, 
                shows=True, 
                trims=True,
                timeframe=[0.8,9], 
                detrend=True,
                spectre=True,
                cor=True,
                out=False,
                sg=True) 

# Adelta fibers
file_read.load_exp(exp_num=10, 
                band=False,
                low=False, #True : only fmax is needed 
                order=3,
                notch=False, 
                fmin=0.1,
                fmax=100, 
                Q=1, 
                shows=True, 
                barre_a_delta=False,
                barre_b=False,
                barre_c=False,
                barre_grosse_fibre=True, 
                trims=True,
                timeframe=[1.5,3], 
                detrend=True,
                spectre=False,
                cor=True,
                out=False,
                sg=False)
'''
