from cProfile import label
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir) 
from signal_processing.processing.filters import *
from signal_processing.processing.spectral import *
from signal_processing.processing.trimming import *
from signal_processing.processing.correlation import *
import glob
import os
from scipy.interpolate import interp1d
from signal_processing.processing import *
import glob
import os
from signal_processing.file_read import load_header,load_path
from scipy.signal import welch
import math

nperseg=64
nfft = 1000

DataMemory = {}

experience_table = [
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1127', '00011127_header.txt'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1128', '00011128_header.txt'),  
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1130SansStimSansFiltre', '00011130_header.txt'),
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1130StimSansFiltre', '00011130_header.txt'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1132SANSSTIMFILTRE', '00011132_header.txt'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1132STIMFILTRE', '00011132_header.txt'),  
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1133SANSSTIMFILTREREfCOMMUN', '00011133_header.txt'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience1', 'header.txt'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience2', '00011203_header.txt'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience3', '00011204_header.txt'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience4', '00011205_header.txt')]

path_table = [
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1127'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1128'),  
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1130SansStimSansFiltre'),
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1130StimSansFiltre'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1132SANSSTIMFILTRE'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1132STIMFILTRE'),  
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1133SANSSTIMFILTREREfCOMMUN'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience1'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience2'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience3'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience4')]

liste_experience = glob.glob(os.path.join('.', 'Data', 'StimPetiteFibre1907', '**'))
liste_graph_big = glob.glob(os.path.join('.', 'Data', 'RecordsGrossesFibres1607', '**', '*.txt'))

# Aucune légende disponible pour ces expériences, il manque aussi une header pour Nouveau dossier(2)
# 3 expériences
experience_big_table = [
    os.path.join('.', 'Data', 'RecordsGrossesFibres1607', 'Nouveau dossier', '00011121_header.txt'),
    os.path.join('.', 'Data', 'RecordsGrossesFibres1607', 'Nouveau dossier (3)', '00011113_header.txt'),
    os.path.join('.', 'Data', 'RecordsGrossesFibres1607', 'Nouveau dossier (4)', '00011117_header.txt')]

experience_sur_place = ['D:/ALL/PA/Project/projet-douleur/Data/experiencesurplace/00011200_header.txt']

fs = 24000
experience = experience_table
def get_psd(exp_num):
    path = path_table[exp_num]
    headerpath = experience[exp_num]
    header = load_header(headerpath)
    distinct_times = list(set(list(header.RecordingTime)))
    data = {1: [], 2: [], 3: [], 4: []}
    for e in os.walk("./Data/Memory"): 
        l=e[2]       #l contient tous les elements de data/memory
        if not str(exp_num)+".csv" in l:
            for time in distinct_times:
                header_small = header[header["RecordingTime"] == time]
                for index, row in header_small.iterrows():
                    x, y = load_path(os.path.join(path, row["Filename"]))
                    # ATTENTION Reste à rajouter le channel en legende
                    if data[int(row['Channel'])] == []:
                        for i,el in enumerate(y):
                            data[int(row['Channel'])].append(el)
                    else:
                        for i,el in enumerate(y):
                            if data[int(row['Channel'])][i]:
                                data[int(row['Channel'])][i] += el
            with open("./Data/Memory/"+str(exp_num)+".csv","w") as f:
                for key in data.keys():
                    f.write("%s; %s\n" % (key, data[key]))
        else: 
            data={}
            with open("./Data/Memory/"+str(exp_num)+".csv","r") as f:      #on crée le fichier
                l=f.readlines()
                for i in range(4):
                    data[i+1]=[float(_) for _ in l[i][4:-2].split(', ')]
    psd={}
    x = np.array(range(1,7201))/fs
    for key in data:
        if key!=4:
            psd[key]=welch(data[key],fs,nperseg=nperseg,nfft=nfft)
    return psd

# Il faudrait aussi essayer ave le filtre de Wiener de scipy peut-être
def get_filter(psd_noise,psd_signal):
    dic_H={}
    for key in psd_noise:
        dic_H[key]=[psd_noise[key][0],[1/(1+psd_noise[key][1][i]/psd_signal[1][i]) for i in range(len(psd_noise[key][0]))]]
    return dic_H

x = np.array(range(1,7201))/fs

def sin(ampli,freq):
    return [ampli*math.sin(2*math.pi*freq*k/fs) for k in x]

def gauss(ampli,std_deviation,avg):
    return [ampli*(1/(std_deviation*(2*math.pi)**0.5))*math.exp(-0.5*((k/fs-avg)/std_deviation)**2) for k in x]

psd_noise=get_psd(4) #Ne prend pas en compte le contenu spectrale de l'artefact de stimulation
psd_signal=welch(gauss(1,0.005,0.0085),fs,nperseg=nperseg,nfft=nfft)
#psd_signal=welch(sin(10,5000),fs,nperseg=nperseg,nfft=nfft)
psd_signalwithnoise=get_psd(5)

dic_H=get_filter(psd_noise,psd_signal) # filtre de Wiener
# on pourrait trouver appliquer ce filtre au signal, mais les hypothèses concernant le signal sont trop fortes

fig, axs = plt.subplots(2, 2)
max_pt =210
for key in psd_noise:
    axs[0, 0].plot(psd_noise[key][0][:max_pt], psd_noise[key][1][:max_pt],marker='x')
axs[0, 0].set_title('DSP du bruit')
axs[0, 0].set(xlabel='Fréquence (Hz)', ylabel='Puissance (mV**2/Hz)')
axs[1, 0].plot(psd_signal[0][:max_pt], psd_signal[1][:max_pt])
axs[1, 0].set_title('DSP du signal (modèle gaussien)')
axs[1, 0].set(xlabel='Frequence (Hz)', ylabel='Puissance (mV**2/Hz)')
for key in dic_H:
    axs[0, 1].plot(dic_H[key][0][:max_pt],dic_H[key][1][:max_pt])
axs[0, 1].set_title('Filtre de Wiener')
axs[0, 1].set(xlabel='Fréquence (Hz)', ylabel='Puissance (mV**2/Hz)')
for key in psd_signalwithnoise:
    axs[1, 1].plot(psd_signalwithnoise[key][0][:max_pt], psd_signalwithnoise[key][1][:max_pt],marker='x')
axs[1, 1].set_title('DSP signal bruité')
axs[1, 1].set(xlabel='Frequence (Hz)', ylabel='Puissance (mV**2/Hz)')
plt.tight_layout()
plt.plot()
plt.show()
