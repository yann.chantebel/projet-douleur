
from scipy import signal
import matplotlib.pyplot as plt
from scipy import fftpack
import numpy as np
from math import *

def fourier(y,fs):                      # fourier transform; frequency analysis
    '''
    Plots the fourier transform of a given signal in frequency domain: 
        Parameters : 
            y(list): the chosen data from the experiment
        Returns :
            None
    '''
    
    y1 = np.array(y)
    # y1 += np.array([1/10*np.sin(2*np.pi*8000*x/fs) for x in range(y1.size)])          #Test pour voir si la TdF marche bien (on attent un pic à 8000)

    xf = fftpack.rfft(y1)
    freqs = fftpack.rfftfreq(y1.size, d=1/fs)

    plt.figure()
    plt.subplot(121)
    plt.title('Signal to analyse')
    plt.xlabel('time(ms)')
    plt.ylabel('amplitude')
    plt.plot([k/fs for k in range(y1.size)],y1)
    plt.subplot(122)
    plt.plot(freqs, abs(xf))
    plt.title('characteristic on frequency domain')
    plt.xlabel('frequency/Hz')
    plt.ylabel('amplitude')
    plt.show()
    return None


fourier([sin(2*pi*10*x/50) for x in range(-100,100)],50)        #test Sin
f=lambda x: 1 if x<=1 and x>=-1 else 0                        #test creneau
fourier([f(x/10) for x in range(-1000,1000)],10)