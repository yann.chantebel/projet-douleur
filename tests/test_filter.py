from matplotlib.pyplot import xlabel
import scipy.signal as signal
import pylab as plt
import numpy as np
import math
import random
from scipy import fftpack

fs = 24000
points=int((21-3)*(fs/1000))*1000
def gauss(ampli,std_deviation,avg):
    return [ampli*(1/(std_deviation*(2*math.pi)**0.5))*math.exp(-0.5*((x/fs-avg)/std_deviation)**2) for x in range(points)]

def sin(ampli,freq):
    return [ampli*math.sin(2*math.pi*freq*x/fs) for x in range(points)]

def filt(list, max_freq,N=2, fs=24000):
    sos = signal.butter(N, max_freq, 'lowpass', fs=fs,output='sos')  
    filteredData = signal.sosfiltfilt(sos, list) 
    return filteredData


def test_filt(test_signal,max_freq,N=2,noise_ratio=0.2,fourier_max=1000):
    ## C'est pas tout à fait le rapport signal bruit,on prend le max, ça reste une fonction de test rudimentaire
    output=[0 for i in range(points)] 
    for sig in test_signal:
        output=[output[x]+sig[x] for x in range(points)]
    if noise_ratio!=0:
        maximum=max(output)
        output=[output[x]+ 2*(random.random()-0.5)*maximum*noise_ratio for x in range(points)]
    time=[x/fs for x in range(points)] 
    fig, axs=plt.subplots(2, 2)
    four= fourier(output)
    axs[0, 0].plot(time, output)
    axs[0, 0].set_title('Signal Théorique (Gaussienne sur 15 ms)')
    axs[0, 0].set(xlabel='Temps', ylabel='Amplitude')
    axs[0, 0].grid()
    axs[0, 1].plot(four[0][:fourier_max], four[1][:fourier_max], 'tab:orange')
    axs[0, 1].set_title('Fourier Original ')
    axs[0, 1].set(xlabel='Fréquence (Hz)', ylabel='Amplitude')
    axs[0, 1].grid()
    signal_filt=filt(output,max_freq,N)
    axs[1, 0].plot(time, signal_filt, 'tab:green')
    axs[1, 0].set_title('Signal filtré à '+str(max_freq)+' Hz')
    axs[1, 0].set(xlabel='Temps', ylabel='Amplitude')
    axs[1, 0].grid()
    four= fourier(signal_filt)
    axs[1, 1].plot(four[0][:fourier_max], four[1][:fourier_max], 'tab:red')
    axs[1, 1].set_title('Fourier après filtrage')
    axs[1, 1].set(xlabel='Frequence (Hz)', ylabel='Amplitude')
    axs[1, 1].grid()
    plt.tight_layout()
    plt.plot()
    plt.tight_layout()
    plt.show()
    return 


def fourier(y):                      # fourier transform; frequency analysis
    '''
    Plots the fourier transform of a given signal in frequency domain: 
        Parameters : 
            y(list): the chosen data from the experiment
        Returns :
            None
    '''
    fs = 24000
    y1 = np.array(y)    
    xf = fftpack.rfft(y1,y1.size)
    freqs = fftpack.rfftfreq(y1.size, d=1/fs)
    return [freqs,abs(xf)]


'''
Exemple :
test_signal=[sin(10,500),sin(1,600),sin(100,3000),gauss(10,0.02,0.1)]
test_filt(test_signal,100,2,0.2,2500)

ou  (plus proche de notre situation)
test_signal=[sin(10,7),sin(40,35),gauss(10,0.038,0.12)]
test_filt(test_signal,20,2,0.7,50)

ou (ce qui se passe quand on filtre dans une gaussienne)
test_signal=[gauss(1,0.005,0.85)]
test_filt(test_signal,5,2,0,500)
'''

test_signal=[gauss(1,0.003,0.118)]
test_filt(test_signal,70,5,0,5000)