from scipy import signal as s
import matplotlib.pyplot as plt
import random as rd
import matplotlib.pyplot as plt
import math 
import scipy.stats
from scipy import signal
import numpy as np
from turtle import position


def correlation(x,y1,y2):
    y=list(s.correlate(y1,y2))
    print("L'ecart calculé grâce à la correlation est: " + str(len(y)//2-y.index(max(y))))
    return None

def test_corre():               #on test plein de correlation ("taille" paires de signaux)
    n=5
    taille = 30
    for i in range(taille-10):
        ecart = i
        y1 = [rd.randint(0,n) for k in range(0)]+[0,2,7,10,12,13,12,5,2,0]+[rd.randint(0,n) for k in range(taille)]
        y2 = [rd.randint(0,n) for k in range(ecart)]+[0,2,7,10,12,13,12,5,2,0]+[rd.randint(0,n) for k in range(taille-ecart)]
        x = range(len(y1))
        print("L'écart test est: "+str(ecart), end=". ")
        correlation(x,y1,y2)

def print_corre(taille, ecart):                     #On test une seule correlation
    fig, axs = plt.subplots(2,2)
    n=5
    y1 = [rd.randint(0,n) for k in range(0)]+[0,2,7,10,12,13,12,5,2,0]+[rd.randint(0,n) for k in range(taille)]                 #voici les 2 signaux qu'on choisit de convoluer
    y2 = [rd.randint(0,n) for k in range(ecart)]+[0,2,7,10,12,13,12,5,2,0]+[rd.randint(0,n) for k in range(taille-ecart)]
    x = range(len(y1))
        

    y=list(s.correlate(y1,y2))
    print("L'ecart calculé grâce à la correlation est: " + str(len(y)//2-y.index(max(y))))

    fig.suptitle('Nombre de colonisé et infecté en fonction du temps')
    axs[0][0].plot(x, y1, label="Courbe1", marker="o", color="red")
    axs[0][0].set_title("Courbe1")
    axs[0][1].plot(x, y2, label="Courbe2", marker="o", color="b")
    axs[0][1].set_title("Courbe2")
    axs[1][0].plot(range(len(y)), y, label="Courbe1", marker="o", color="purple")
    axs[1][0].set_title("Signal corrélé")
    plt.show()

print_corre(100,30)


# 2 sinusoïdes identiques 
x_min = 0
x_max = 65
nb = 300
f= nb/(x_max-x_min)
t = np.linspace(x_min/f, x_max/f, nb)

y_sinus=[]
for abscisse in t : 
    y_sinus.append(math.sin(2*abscisse))
y_correlate_s=list((signal.correlate(y_sinus,y_sinus)))
tc=[(i-len(y_correlate_s)/2)/f for i in range(len(y_correlate_s))]


fig = plt.figure(1, figsize=(16,5)) 

ax1 = plt.subplot(131)
ax1.set_title("Sinusoïde 1 ",fontsize=17)
ax1.plot(t,y_sinus, "blue")
ax1.set_xlabel('Time(s)',fontsize=13)
ax1.set_ylabel('Amplitude',fontsize=13)
ax1.plot()
ax1.grid(True)

ax2 = plt.subplot(132)
ax2.set_title("Sinusoïde 1",fontsize=17)
ax2.plot(t,y_sinus, "blue")
ax2.plot()
ax2.grid(True)

ax2 = plt.subplot(133)
ax2.set_title("Correlation between the both sinusoïdes",fontsize=17)
ax2.plot(tc,y_correlate_s, "blue")
ax2.plot()
ax2.grid(True)

plt.suptitle('Correlation between two identicals sinusoïdes',fontsize=20)
fig.tight_layout()
plt.show()

  
# 2 sinusoïdes décalées 

x_min = 0
x_max = 65
nb = 300
f= nb/(x_max-x_min)
t = np.linspace(x_min/f, x_max/f, nb)
decalage=2

y_sinus=[]
y_sinus1=[]
for abscisse in t : 
    y_sinus.append(math.sin(2*abscisse))
    y_sinus1.append(math.sin(2*(abscisse+decalage)))
y_correlate_s=list((signal.correlate(y_sinus,y_sinus1)))
tc=[(i-len(y_correlate_s)/2)/f for i in range(len(y_correlate_s))]


fig = plt.figure(1, figsize=(16,5)) 

ax1 = plt.subplot(131)
ax1.set_title("Sinusoïde 1 ",fontsize=17)
ax1.plot(t,y_sinus, "blue")
ax1.set_xlabel('Time(s)',fontsize=13)
ax1.set_ylabel('Amplitude',fontsize=13)
ax1.plot()
ax1.grid(True)

ax2 = plt.subplot(132)
ax2.set_title("Sinusoïde 1",fontsize=17)
ax2.plot(t,y_sinus1, "blue")
ax2.plot()
ax2.grid(True)

ax2 = plt.subplot(133)
ax2.set_title("Correlation between the both sinusoïdes",fontsize=17)
ax2.plot(tc,y_correlate_s, "blue")
ax2.plot()
ax2.grid(True)

plt.suptitle('Correlation between two delayed sinusoïdes',fontsize=20)
fig.tight_layout()
plt.show()

# 2 gaussiennes 

x_min = 0
x_max = 65
nb = 300
f= nb/(x_max-x_min)
t = np.linspace(x_min/f, x_max/f, nb)
mean = 8.0 
std = 2.0

y_gaussienne = scipy.stats.norm.pdf(t,mean,std)
y_correlate_g=signal.correlate(y_gaussienne,y_gaussienne)
tc=[(i-len(y_correlate_g)/2)/f for i in range(len(y_correlate_g))]


fig = plt.figure(1, figsize=(16,5)) 

ax1 = plt.subplot(131)
ax1.set_title("Gaussian 1 ",fontsize=17)
ax1.plot(t,y_gaussienne, "blue")
ax1.set_xlabel('Time(s)',fontsize=13)
ax1.set_ylabel('Amplitude',fontsize=13)
ax1.plot()
ax1.grid(True)

ax2 = plt.subplot(132)
ax2.set_title("Gaussian 1",fontsize=17)
ax2.plot(t,y_gaussienne, "blue")
ax2.plot()
ax2.grid(True)

ax2 = plt.subplot(133)
ax2.set_title("Correlation between the both gaussians",fontsize=17)
ax2.plot(tc,y_correlate_g, "blue")
ax2.plot()
ax2.grid(True)

plt.suptitle('Correlation between two identicals gaussians',fontsize=20)
fig.tight_layout()
plt.show()

# 2 gaussiennnes décalées

x_min = 0
x_max = 65
nb = 300
f= nb/(x_max-x_min)
t = np.linspace(x_min/f, x_max/f, nb)
mean = 4.0
mean2 = 8.0
std = 2.0

y_gaussienne = scipy.stats.norm.pdf(t,mean,std)
y_gaussienne2 = scipy.stats.norm.pdf(t,mean2,std)
y_correlate_g=signal.correlate(y_gaussienne,y_gaussienne2)
tc=[(i-len(y_correlate_g)/2)/f for i in range(len(y_correlate_g))]


fig = plt.figure(1, figsize=(16,5)) 

ax1 = plt.subplot(131)
ax1.set_title("Gaussian 1 ",fontsize=17)
ax1.plot(t,y_gaussienne, "blue")
ax1.set_xlabel('Time(s)',fontsize=13)
ax1.set_ylabel('Amplitude',fontsize=13)
ax1.plot()
ax1.grid(True)

ax2 = plt.subplot(132)
ax2.set_title("Gaussian 2",fontsize=17)
ax2.plot(t,y_gaussienne2, "blue")
ax2.plot()
ax2.grid(True)

ax2 = plt.subplot(133)
ax2.set_title("Correlation between the both gaussians",fontsize=17)
ax2.plot(tc,y_correlate_g, "blue")
ax2.plot()
ax2.grid(True)

plt.suptitle('Correlation between two delayed gaussians',fontsize=20)
fig.tight_layout()
plt.show()

# 2 gaussiennnes décalées avec du bruit aléatoire

x_min = 0
x_max = 65
nb = 300
f= nb/(x_max-x_min)
t = np.linspace(x_min/f, x_max/f, nb)
mean = 4.0
mean2 = 8.0
std = 2.0

random_numbers = np.random.uniform(low=0, high=0.175, size=len(t)).tolist()
random_numbers2 = np.random.uniform(low=0, high=0.175, size=len(t)).tolist()
y_gaussienne = scipy.stats.norm.pdf(t,mean,std)+random_numbers
y_gaussienne2 = scipy.stats.norm.pdf(t,mean2,std)+random_numbers2
y_correlate_g=signal.correlate(y_gaussienne,y_gaussienne2)
tc=[(i-len(y_correlate_g)/2)/f for i in range(len(y_correlate_g))]


fig = plt.figure(1, figsize=(16,5)) 

ax1 = plt.subplot(131)
ax1.set_title("Gaussian 1 ",fontsize=17)
ax1.plot(t,y_gaussienne, "blue")
ax1.set_xlabel('Time(s)',fontsize=13)
ax1.set_ylabel('Amplitude',fontsize=13)
ax1.plot()
ax1.grid(True)

ax2 = plt.subplot(132)
ax2.set_title("Gaussian 2",fontsize=17)
ax2.plot(t,y_gaussienne2, "blue")
ax2.plot()
ax2.grid(True)

ax2 = plt.subplot(133)
ax2.set_title("Correlation between the both gaussians",fontsize=17)
ax2.plot(tc,y_correlate_g, "blue")
ax2.plot()
ax2.grid(True)

plt.suptitle('Correlation between two delayed gaussians and different noise',fontsize=20)
fig.tight_layout()
plt.show()


# 2 gaussiennes décalées avec du bruit identique sur les deux

x_min = 0
x_max = 65
nb = 300
f= nb/(x_max-x_min)
t = np.linspace(x_min/f, x_max/f, nb)
mean = 4.0
mean2 = 8.0
std = 2.0

random_numbers = np.random.uniform(low=0, high=0.175, size=len(t)).tolist()
y_gaussienne = scipy.stats.norm.pdf(t,mean,std)+random_numbers
y_gaussienne2 = scipy.stats.norm.pdf(t,mean2,std)+random_numbers
y_correlate_g=signal.correlate(y_gaussienne,y_gaussienne2)
tc=[(i-len(y_correlate_g)/2)/f for i in range(len(y_correlate_g))]


fig = plt.figure(1, figsize=(16,5)) 

ax1 = plt.subplot(131)
ax1.set_title("Gaussian 1 ",fontsize=17)
ax1.plot(t,y_gaussienne, "blue")
ax1.set_xlabel('Time(s)',fontsize=13)
ax1.set_ylabel('Amplitude',fontsize=13)
ax1.plot()
ax1.grid(True)

ax2 = plt.subplot(132)
ax2.set_title("Gaussian 2",fontsize=17)
ax2.plot(t,y_gaussienne2, "blue")
ax2.plot()
ax2.grid(True)

ax2 = plt.subplot(133)
ax2.set_title("Correlation between the both gaussians",fontsize=17)
ax2.plot(tc,y_correlate_g, "blue")
ax2.plot()
ax2.grid(True)

plt.suptitle('Correlation between two delayed gaussians and same noise',fontsize=20)
fig.tight_layout()
plt.show()



# 2 gaussiennes identiques avec petite gaussienne décalée 

x_min = 0
x_max = 65
nb = 300
f= nb/(x_max-x_min)
t = np.linspace(x_min/f, x_max/f, nb)
mean = 4.0
mean2 = 1.7
mean3 = 2.1
std = 2.0
std2 = 0.5

y_gaussienne = 15*(scipy.stats.norm.pdf(t,mean,std)) + 0.5*scipy.stats.norm.pdf(t,mean2,std2)
y_gaussienne2 = 15*scipy.stats.norm.pdf(t,mean,std) + 0.5*scipy.stats.norm.pdf(t,mean3,std2)
y_correlate_g=signal.correlate(y_gaussienne,y_gaussienne2)
tc=[(i-len(y_correlate_g)/2)/f for i in range(len(y_correlate_g))]


fig = plt.figure(1, figsize=(16,5)) 

ax1 = plt.subplot(131)
ax1.set_title("Gaussian 1 ",fontsize=17)
ax1.plot(t,y_gaussienne, "blue")
ax1.set_xlabel('Time(s)',fontsize=13)
ax1.set_ylabel('Amplitude',fontsize=13)
ax1.plot()
ax1.grid(True)

ax2 = plt.subplot(132)
ax2.set_title("Gaussian 2",fontsize=17)
ax2.plot(t,y_gaussienne2, "blue")
ax2.plot()
ax2.grid(True)

ax2 = plt.subplot(133)
ax2.set_title("Correlation between the both gaussians",fontsize=17)
ax2.plot(tc,y_correlate_g, "blue")
ax2.plot()
ax2.grid(True)

plt.suptitle('Correlation between two gaussians with small delayed gaussian',fontsize=20)
fig.tight_layout()
plt.show()



