def trim(x, data, start=12, end=22,fs=24000):
    '''
    Returns a trimmed signal at the beginning and at the end
        Parameters:
            x : x axis to plot
            data (dictionnary) : initial plot with some zeroes at the beginning and at the end'
                shape:  data = {1: y1 2: y2, 3: y3, 4: y4} where yi are list of values
            start (int) : time in ms of the beginning to cut out
            end (int) : time in ms of the end to cut out, obvious precautions should be taken to avoid cutting too much of the signal
            average (boolean): Set to True to set the average of all signal to 0
        Returns :
            x,data : same plot without the zeroes
    '''

    start = int(start*(fs/1000))
    end = int(end*(fs/1000))
    x = x[start:end]
    for key in data:
        data[key] = data[key][start:end]
    return x, data