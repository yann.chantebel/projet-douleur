from scipy import signal




def low_pass(data, N=2, fmax=2160, fs=24000):
    '''
    Applies bandpass filter to data: 
        Parameters : 
            data (list) : evenly spaced datapoints
            N (int) : order of the filter  
            fmax (int) : top value frequency
            fs (int) : sampling frequency
        Returns :
            filteredData (list) : evenly spaced datapoints, filtered by the bandpass filter
    '''
    sos = signal.butter(N, fmax, 'lowpass', fs=fs,output='sos')  
    filteredData = signal.sosfiltfilt(sos, data) 
    return filteredData

def notch_filter(data, w0, Q, fs=24000):
    '''
    Applies notch filter to data: 
        Parameters : 
            data (list) : evenly spaced datapoints
            w0 (int) : caracteristic frequency to cut out (here it will often be 50Hz)
            Q (int) : quality factor 
            fs (int) : sampling frequency, conditions the caracteristic frquency's unit
        Returns :
            filteredData (list) : evenly spaced datapoints, filtered by the notch filter
    '''
    b, a = signal.iirnotch(w0, Q, fs)
    filteredData = signal.filtfilt(b, a, data)
    return filteredData

def band_pass(data, N=2, fmin=240, fmax=2160, fs=24000):
    '''
    Applies bandpass filter to data:
        Parameters :
            data (list) : evenly spaced datapoints
            N (int) : order of the bandpass filter
            [fmin,fmax] ([int,int]) : -3dB bandwidth
            fs (int) : sampling frequency, conditions the bandwidth's unit
        Returns :
            filteredData (list) : evenly spaced datapoints, filtered by the bandpass filter
    '''

    sos= signal.butter(N, [fmin, fmax], 'bandpass',
                         fs=fs,output='sos')  
    # b, a = signal.butter(8, 0.006, 'highpass')
    filteredData = signal.sosfiltfilt(sos, data) 
    return filteredData

def detrender(y):
    return signal.detrend(y,type='linear')