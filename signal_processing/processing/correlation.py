from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
from signal_processing.processing.filters import low_pass

def correlation_all(data,exp_num,fs=24000):
    '''
    Takes an arbitrary channel as reference and computes the delay between this channel and all other, giving a good estimates of the time delay between each electrodes
        Parameters:
            x (list) : evenly spaced datapoints
            data (dictionnary) : electrode signals sorted by channel
        Returns:
            
    '''
    y={}
    delay_list=[]
    
    for key in data:
        y[key]=list(signal.correlate(data[1], data[key]))  # Correlation entre la 1ère électrode et les autres, dictionnaire de 4 éléments 
        delay=((np.argmax(y[key])-len(y[key])/2)/fs)*1000 # Délai entre les électrodes en milliseconde
        delay_list.append(delay)
    print(delay_list)
    delay_list=([el-min(delay_list) for el in delay_list]) # ? Déjà sensé être 0
    delay_list.sort() # On trie la liste pour être sur qu'on part de l'électrode la plus proche et qu'on va vers la plus éloignée 
    
    print("Les écarts entre les signaux calculés grâce à la correlation sont: " + str(delay_list)+" ms")
    delays=[delay_list[i+1]-delay_list[i] for i in range(len(delay_list)-1)][0:1]
    print(delays)
    
    fig = plt.figure(1, figsize=(15, 10)) 
 
    
    # Plage de données utilisée pour tracer la corrélation sur toute la longueur du signal étudié
    
    y_x={}
    y_x[1]=[1000*(i-len(y[1])/2)/fs for i in range(len(y[1]))]
    y_x[2]=[1000*(i-len(y[2])/2)/fs for i in range(len(y[2]))]
    y_x[3]=[1000*(i-len(y[3])/2)/fs for i in range(len(y[3]))]
    y_x[4]=[1000*(i-len(y[4])/2)/fs for i in range(len(y[4]))]
    
    
    ax1 = plt.subplot(441)
    ax1.set_title("1 and 1")
    ax1.plot(y_x[1], y[1], "blue")
    ax1.grid(True)
    
    ax2 = plt.subplot(442)
    ax2.set_title('1 and 2')
    ax2.plot(y_x[2], y[2], "gold")
    ax2.grid(True)
    
    ax3 = plt.subplot(443)
    ax3.set_title('1 and 3')
    ax3.plot(y_x[3], y[3], "green")
    ax3.grid(True)
    
    ax4 = plt.subplot(444)
    ax4.set_title('Superposition')
    ax4.plot(y_x[3], y[3], "green")
    ax4.plot(y_x[1], y[1], "blue")
    ax4.plot(y_x[2], y[2], "gold")
    ax4.grid(True)
    
    
     # Selection de la bonne plage de donnée intéressante pour zoomer à l'endroit où on doit observer notre signal
    
    if exp_num in [1,2,3,4,5,6,7,8,9]:
        for key in data:
            y[key]= y[key][int(len(y[key])/2 - 5/1000*fs) :int(len(y[key])/2 - 0.2/1000*fs)+1 ]
            y_x[key]=[ -5 + i*1000/fs for i in range(len(y[key]))]
            #plt.text(6,35, 'we expect a delay of 0.4-2ms between electrode 1 and 2 and 0.8-4ms between 1 and 3')
    if exp_num==10 : 
        for key in data:
            y[key]= y[key][int(len(y[key])/2 - 0.60/1000*fs) +1 : int(len(y[key])/2 + 0.1/1000*fs) ]
            y_x[key]=[ -0.60 + i*1000/fs for i in range(len(y[key]))]
            # The signal for the first electrode is supposed to arrive at 1.5ms and 2.8ms
            #plt.text(6,35, 'we expect a delay of 0.15-0.28 ms between 1 and 2 and 0.3-0.6 between 1 and 3')
                
          
    # Calcul de la dérivée
    diff_y={}

    for key in y:
        diff_y[key]=np.diff(low_pass(y[key],N=1,fmax=500,fs=fs),1) 

    '''
    derivee_y={}
    derivee_x={}
    for key in data :
        derivee_y[key]=[]
        derivee_x[key]=[0]*(len(y_x[key])-1)
        derivee_x[key][0]=y_x[key][0]
        derivee_x[key][1]=y_x[key][1]
        for i in range(len(y_x[key])-1):
            derivee_y[key].append((y[key][i+1]-y[key][i])/(y_x[key][i+1]-y_x[key][i]))
            if i < (len(derivee_x[key])):
                derivee_x[key][i]=(y_x[key][i+1]-y_x[key][i])/2 + derivee_x[key][i-1]
        derivee_y[key].append(derivee_y[key][-1])
    '''
    #

    # Option d'affichage figure zoomée 
            
    ax1 = plt.subplot(445)
    ax1.set_title("1 and 1 (zoom)")
    ax1.plot(y_x[1], y[1], "blue", marker='x')
    
    ax1.grid(True)
    
    ax2 = plt.subplot(446)
    ax2.set_title('1 and 2 (zoom)')
    ax2.plot(y_x[2], y[2], "gold", marker='x')
    ax2.grid(True)
    
    ax3 = plt.subplot(447)
    ax3.set_title('1 and 3 (zoom)')
    ax3.plot(y_x[3], y[3], "green", marker='x')
    ax3.grid(True)
    
    ax4 = plt.subplot(448)
    ax4.set_title('Superposition')
    ax4.plot(y_x[3], y[3], "green")
    ax4.plot(y_x[1], y[1], "blue")
    ax4.plot(y_x[2], y[2], "gold")
    ax4.grid(True)
    
    if exp_num in [0,1,2,3,4,5,6,7,8,9]:
        plt.axvline(x=-0.4,color='gold', linestyle='--')
        plt.axvline(x=-2,color='gold', linestyle='--')
        plt.axvline(x=-0.8,color='green', linestyle='--')
        plt.axvline(x=-4,color='green', linestyle='--')
    
    if exp_num==10 : 
        plt.axvline(x=-0.15,color='gold', linestyle='--')
        plt.axvline(x=-0.28,color='gold', linestyle='--')
        plt.axvline(x=-0.3,color='green', linestyle='--')
        plt.axvline(x=-0.56,color='green', linestyle='--')
    
    
    ax5 = plt.subplot(4,4,9)
    ax5.set_title('Derivé ordre 1 signal 1')
    ax5.plot(y_x[1][:-1], diff_y[1], "blue", marker='x')
    ax5.grid(True)
    
    ax6 = plt.subplot(4,4,10)
    ax6.set_title('Derivé ordre 1 signal 2 ')
    ax6.plot(y_x[2][:-1], diff_y[2], "gold", marker='x')
    ax6.grid(True)
    
    ax7 = plt.subplot(4,4,11)
    ax7.set_title('Derivé ordre 1 signal 3 ')
    ax7.plot(y_x[3][:-1], diff_y[3] , "green", marker='x')
    ax7.grid(True)
    
    ax8 = plt.subplot(4,4,12)
    ax8.set_title('Derivés ordre 1')
    ax8.plot(y_x[1][:-1], diff_y[1], "blue")
    ax8.plot(y_x[2][:-1], diff_y[2], "gold")
    ax8.plot(y_x[3][:-1], diff_y[3], "green")
    ax8.grid(True)

    if exp_num in [0,1,2,3,4,5,6,7,8,9]:
        plt.axvline(x=-0.4,color='gold', linestyle='--')
        plt.axvline(x=-2,color='gold', linestyle='--')
        plt.axvline(x=-0.8,color='green', linestyle='--')
        plt.axvline(x=-4,color='green', linestyle='--')
    
    if exp_num==10 : 
        plt.axvline(x=-0.15,color='gold', linestyle='--')
        plt.axvline(x=-0.28,color='gold', linestyle='--')
        plt.axvline(x=-0.3,color='green', linestyle='--')
        plt.axvline(x=-0.56,color='green', linestyle='--')


    for key in y:
        
        diff_y[key]=np.diff(low_pass(diff_y[key],N=1,fmax=500,fs=fs),1) 

    ax9 = plt.subplot(4,4,13)
    ax9.set_title('Derivé ordre 2 signal 1')
    ax9.plot(y_x[1][:-2], diff_y[1], "blue", marker='x')
    ax9.grid(True)
    
    ax10 = plt.subplot(4,4,14)
    ax10.set_title('Derivé ordre 2 signal 2')
    ax10.plot(y_x[2][:-2], diff_y[2], "gold", marker='x')
    ax10.grid(True)
    
    ax11 = plt.subplot(4,4,15)
    ax11.set_title('Derivé ordre 2 signal 3')
    ax11.plot(y_x[3][:-2], diff_y[3] , "green", marker='x')
    ax11.grid(True)
    
    ax12= plt.subplot(4,4,16)
    ax12.set_title('Derivés ordre 2')
    ax12.plot(y_x[1][:-2], diff_y[1], "blue")
    ax12.plot(y_x[2][:-2], diff_y[2], "gold")
    ax12.plot(y_x[3][:-2], diff_y[3], "green")
    ax12.grid(True)


    if exp_num in [0,1,2,3,4,5,6,7,8,9]:
        plt.axvline(x=-0.4,color='gold', linestyle='--')
        plt.axvline(x=-2,color='gold', linestyle='--')
        plt.axvline(x=-0.8,color='green', linestyle='--')
        plt.axvline(x=-4,color='green', linestyle='--')
    
    if exp_num==10 : 
        plt.axvline(x=-0.15,color='gold', linestyle='--')
        plt.axvline(x=-0.28,color='gold', linestyle='--')
        plt.axvline(x=-0.3,color='green', linestyle='--')
        plt.axvline(x=-0.56,color='green', linestyle='--')
        
    # On fait le réglage des deux sets de deux barres
    # Le délai entre les deux barres gold correspond au délai attendu entre l'électrode 1 : bleue et la 2 : gold
    # Le délai entre les deux barres green correspond au délai attendu  entre l'électrode 1 : bleue  et la 3 : green
    # On devrait trouver un pic entre chacune de ces sets de 2 barres 
    
 
    if type == 'small' :
        ax4 = plt.subplot(248)
        ax4.set_title('1 and 4 (zoom)')
        ax4.plot(y_x[4], y[4], "purple", marker='o')
        ax4.grid(True)
     
    plt.suptitle('Correlation between two electrodes',fontsize=20)
    plt.tight_layout()
    plt.show()

    return delay_list

