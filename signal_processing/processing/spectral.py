import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack



def fourier(y,tmin,fs=24000):                      # fourier transform; frequency analysis
    '''
    Plots the fourier transform of a given signal in frequency domain: 
        Parameters : 
            y(list): the chosen data from the experiment
        Returns :
            None
    '''
    y1 = np.array(y)
    # y1 += np.array([1/10*np.sin(2*np.pi*8000*x/fs) for x in range(y1.size)])          Test pour voir si la TdF marche bien (on attent un pic à 8000)

    xf = fftpack.rfft(y1,2*len(y1))
    freqs = fftpack.rfftfreq(2*y1.size, d=1/fs)

    plt.figure()
    plt.subplot(121)
    plt.title('Signal to analyse')
    plt.xlabel('Time (ms)')
    plt.grid(True)
    plt.ylabel('amplitude')
    plt.grid(True)
    plt.plot([tmin + k/(fs/1000) for k in range(y1.size)],y1)
    plt.subplot(122)
    plt.plot(freqs, abs(xf),marker='x')
    plt.title('Fourier transform')
    plt.xlabel('Frequencies (Hz)')
    plt.xlim(0,500)
    plt.grid(True)
    plt.ylabel('amplitude')
    plt.grid(True)
    plt.show()
    return [freqs, abs(xf)]


def fourier_simple(y,fs=24000):                      # fourier transform; frequency analysis
    '''
    Plots the fourier transform of a given signal in frequency domain: 
        Parameters : 
            y(list): the chosen data from the experiment
        Returns :
            None
    '''
    y1 = np.array(y)    
    xf = fftpack.rfft(y1,2*len(y1))
    freqs = fftpack.rfftfreq(2*len(y1), d=1/fs)
    return [freqs,abs(xf)]