from tkinter import *
from tkinter.filedialog import askopenfilename
import cv2
from sympy import fourier_transform
from fft import *
from file_read import *

liste_graph = glob.glob("./Data/StimPetiteFibre1907/**/*.txt")

def callback(old, new):
    img2 = PhotoImage(file=new)
    old.config(image=img2)
    old.image = img2

def start():
    '''This function is used to call the GUI and start our recognition application 
    '''
    # ceate the window
    root = Tk()
    root.title('Graph of result')
    root.geometry('600x400')
    root.resizable(height = None, width = None)

    # set the background
    photo = PhotoImage(file="./Data/backimage.gif")
    root.background = Label(root, image=photo)
    root.background.pack()

    # organize 3 frame to better construct the main window
    frame1 = Frame(root, height=2, width=2, bg='DarkBlue')
    Label_1 = Label(frame1, text="Use browse to", justify=LEFT, font=(
        "Arial", 15), bg='DodgerBlue', fg="black")
    Label_1.pack(side=LEFT)
    frame1.place(relx=0.4, rely=0.3, anchor=CENTER)

    # place all the label
    part_1 = PhotoImage(file="")
    label_1 = Label(image=part_1)
    label_1.place(relx=0.5, rely=0.5, anchor=CENTER)

    # place the start button
    button_image = PhotoImage(file='./Data/button_2.gif')
    button = Button(image=button_image, command=lambda: browse(root, label_1))
    button.place(relx=0.6, rely=0.3, anchor=CENTER)

    root.mainloop()

def browse(root, label_rec):
    '''This function is connected to the GUI button, it can call the video and 
    start detection and recognition

    Parameters
    ----------
    root : tkinter Tk

    '''
    file1 = askopenfilename()
    file2 = askopenfilename()
    # print('./'+os.path.relpath(file1))
    data_1 = load_exp('./'+os.path.relpath(file1))
    data_2 = load_exp('./'+os.path.relpath(file2))
    # draw_difference(data_1, data_2)

    spectral(data_1[1], data_2[1])

    cv2.waitKey(0)
    cv2.destroyAllWindows()

start()