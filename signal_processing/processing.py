from scipy import signal
import matplotlib.pyplot as plt
from scipy import fftpack
import numpy as np
from math import *


def band_pass(data, N=2, fmin=240, fmax=2160, fs=24000):
    '''
    Applies bandpass filter to data:
        Parameters :
            data (list) : evenly spaced datapoints
            N (int) : order of the bandpass filter
            [fmin,fmax] ([int,int]) : -3dB bandwidth
            fs (int) : sampling frequency, conditions the bandwidth's unit
        Returns :
            filteredData (list) : evenly spaced datapoints, filtered by the bandpass filter
    '''

    sos= signal.butter(N, [fmin, fmax], 'bandpass',
                         fs=fs,output='sos')  
    # b, a = signal.butter(8, 0.006, 'highpass')
    filteredData = signal.sosfiltfilt(sos, data) 
    return filteredData

def low_pass(data, N=2, fmax=2160, fs=24000):
    '''
    Applies bandpass filter to data: 
        Parameters : 
            data (list) : evenly spaced datapoints
            N (int) : order of the filter  
            fmax (int) : top value frequency
            fs (int) : sampling frequency
        Returns :
            filteredData (list) : evenly spaced datapoints, filtered by the bandpass filter
    '''

    sos = signal.butter(N, fmax, 'lowpass', fs=fs,output='sos')  
    filteredData = signal.sosfiltfilt(sos, data) 
    return filteredData


def notch_filter(data, w0, Q, fs=24000):
    '''
    Applies notch filter to data: 
        Parameters : 
            data (list) : evenly spaced datapoints
            w0 (int) : caracteristic frequency to cut out (here it will often be 50Hz)
            Q (int) : quality factor 
            fs (int) : sampling frequency, conditions the caracteristic frquency's unit
        Returns :
            filteredData (list) : evenly spaced datapoints, filtered by the notch filter
    '''
    b, a = signal.iirnotch(w0, Q, fs)
    filteredData = signal.filtfilt(b, a, data)
    return filteredData

#22.5 - 40 = 0.45 - 0.20

def trim(x, data, start=12, end=22):
    '''
    Returns a trimmed signal at the beginning and at the end
        Parameters:
            x : x axis to plot
            data (dictionnary) : initial plot with some zeroes at the beginning and at the end'
                shape:  data = {1: y1 2: y2, 3: y3, 4: y4} where yi are list of values
            start (int) : time in ms of the beginning to cut out
            end (int) : time in ms of the end to cut out, obvious precautions should be taken to avoid cutting too much of the signal
            average (boolean): Set to True to set the average of all signal to 0
        Returns :
            x,data : same plot without the zeroes
    '''
    start = int(start*24)
    end = int(end*24)
    x = x[start:end]
    for key in data:
        data[key] = data[key][start:end]
    return x, data

def detrender(data):

    for key in data:
        data[key] = signal.detrend(data[key],type='linear')

    return data

#def detrend_correlation(data):

'''def correlation(x, y1, y2):  # tested and approved by Kant1
    
    Returns correlation of 2 given signals and prints the calculated delay between them: 
        Parameters : 
            x (list) : evenly spaced datapoints
            y1 (list) : first signal 
            y2 (list) : second signal 
        Returns :
            y (list) : correlated signal
    
    fs = 24000

    y = list(signal.correlate(y1, y2))  # Correlation
    delay=(len(y)//2-list(y).index(max(y)))/fs*1000 #en milliseconde
    print("L'écart calculé grâce à la correlation est: " +
          str(delay)+"ms")

    ax1 = plt.subplot(212)
    ax1.plot(range(len(y)), y, "purple")
    ax1.set_title('Correlation', x=0.12, y=0.8)

    ax2 = plt.subplot(221)
    ax2.plot(x, y1, "r")
    ax2.set_title('Signal 1')

    ax3 = plt.subplot(222)
    ax3.plot(x, y2, "b")
    ax3.set_title('Signal 2')

    plt.show()

    return delay'''

def correlation_all(data,exp_num):
    '''
    Takes an arbitrary channel as reference and computes the delay between this channel and all other, giving a good estimates of the time delay between each electrodes
        Parameters:
            x (list) : evenly spaced datapoints
            data (dictionnary) : electrode signals sorted by channel
        Returns:
            
    '''
    fs = 24000
    y={}
    delay_list=[]
    
    for key in data:
        y[key]=list(signal.correlate(data[1], data[key]))  # Correlation entre la 1ère électrode et les autres, dictionnaire de 4 éléments 
        delay=((np.argmax(y[key])-len(y[key])/2)/fs)*1000 # Délai entre les électrodes en milliseconde
        delay_list.append(delay)
    print(delay_list)
    delay_list=([el-min(delay_list) for el in delay_list]) # ? Déjà sensé être 0
    delay_list.sort() # On trie la liste pour être sur qu'on part de l'électrode la plus proche et qu'on va vers la plus éloignée 
    
    print("Les écarts entre les signaux calculés grâce à la correlation sont: " + str(delay_list)+" ms")
    delays=[delay_list[i+1]-delay_list[i] for i in range(len(delay_list)-1)][0:1]
    print(delays)
    
    fig = plt.figure(1, figsize=(15, 10)) 
 
    
    # Plage de données utilisée pour tracer la corrélation sur toute la longueur du signal étudié
    
    y_x={}
    y_x[1]=[1000*(i-len(y[1])/2)/fs for i in range(len(y[1]))]
    y_x[2]=[1000*(i-len(y[2])/2)/fs for i in range(len(y[2]))]
    y_x[3]=[1000*(i-len(y[3])/2)/fs for i in range(len(y[3]))]
    y_x[4]=[1000*(i-len(y[4])/2)/fs for i in range(len(y[4]))]
    
    
    ax1 = plt.subplot(341)
    ax1.set_title("1 and 1")
    ax1.plot(y_x[1], y[1], "blue")
    ax1.plot()
    ax1.grid(True)
    
    ax2 = plt.subplot(342)
    ax2.set_title('1 and 2')
    ax2.plot(y_x[2], y[2], "gold")
    ax2.grid(True)
    
    ax3 = plt.subplot(343)
    ax3.set_title('1 and 3')
    ax3.plot(y_x[3], y[3], "green")
    ax3.grid(True)
    
    ax4 = plt.subplot(344)
    ax4.set_title('Superposition')
    ax4.plot(y_x[3], y[3], "green")
    ax4.plot(y_x[1], y[1], "blue")
    ax4.plot(y_x[2], y[2], "gold")
    ax4.grid(True)
    
    
     # Selection de la bonne plage de donnée intéressante pour zoomer à l'endroit où on doit observer notre signal
    
    if exp_num in [1,2,3,4,5,6,7,8,9]:
        for key in data:
            y[key]= y[key][int(len(y[key])/2 - 5/1000*fs) :int(len(y[key])/2 - 0.2/1000*fs)+1 ]
            y_x[key]=[ -5 + i*1000/fs for i in range(len(y[key]))]
            #plt.text(6,35, 'we expect a delay of 0.4-2ms between electrode 1 and 2 and 0.8-4ms between 1 and 3')
    if exp_num==10 : 
        for key in data:
            y[key]= y[key][int(len(y[key])/2 - 0.60/1000*fs) +1 : int(len(y[key])/2 + 0.1/1000*fs) ]
            y_x[key]=[ -0.60 + i*1000/fs for i in range(len(y[key]))]
            # The signal for the first electrode is supposed to arrive at 1.5ms and 2.8ms
            #plt.text(6,35, 'we expect a delay of 0.15-0.28 ms between 1 and 2 and 0.3-0.6 between 1 and 3')
                
          
    # Calcul de la dérivée
    derivee_y={}
    derivee_x={}
    for key in data :
        derivee_y[key]=[]
        derivee_x[key]=[0]*(len(y_x[key])-1)
        derivee_x[key][0]=y_x[key][0]
        derivee_x[key][1]=y_x[key][1]
        for i in range(len(y_x[key])-1):
            derivee_y[key].append((y[key][i+1]-y[key][i])/(y_x[key][i+1]-y_x[key][i]))
            if i < (len(derivee_x[key])):
                derivee_x[key][i]=(y_x[key][i+1]-y_x[key][i])/2 + derivee_x[key][i-1]
        derivee_y[key].append(derivee_y[key][-1])
            

    # Option d'affichage figure zoomée 
            
    ax1 = plt.subplot(345)
    ax1.set_title("1 and 1 (zoom)")
    ax1.plot(y_x[1], y[1], "blue", marker='x')
    
    ax1.grid(True)
    
    ax2 = plt.subplot(346)
    ax2.set_title('1 and 2 (zoom)')
    ax2.plot(y_x[2], y[2], "gold", marker='x')
    ax2.grid(True)
    
    ax3 = plt.subplot(347)
    ax3.set_title('1 and 3 (zoom)')
    ax3.plot(y_x[3], y[3], "green", marker='x')
    ax3.grid(True)
    
    ax4 = plt.subplot(348)
    ax4.set_title('Superposition')
    ax4.plot(y_x[3], y[3], "green")
    ax4.plot(y_x[1], y[1], "blue")
    ax4.plot(y_x[2], y[2], "gold")
    ax4.grid(True)
    
    if exp_num in [0,1,2,3,4,5,6,7,8,9]:
        plt.axvline(x=-0.4,color='gold', linestyle='--')
        plt.axvline(x=-2,color='gold', linestyle='--')
        plt.axvline(x=-0.8,color='green', linestyle='--')
        plt.axvline(x=-4,color='green', linestyle='--')
    
    if exp_num==10 : 
        plt.axvline(x=-0.15,color='gold', linestyle='--')
        plt.axvline(x=-0.28,color='gold', linestyle='--')
        plt.axvline(x=-0.3,color='green', linestyle='--')
        plt.axvline(x=-0.56,color='green', linestyle='--')
        
    
    ax5 = plt.subplot(349)
    ax5.set_title('Derive 1')
    ax5.plot(y_x[1], derivee_y[1], "blue", marker='x')
    ax5.grid(True)
    
    ax6 = plt.subplot(3,4,10)
    ax6.set_title('Derive 2')
    ax6.plot(y_x[2], derivee_y[2], "gold", marker='x')
    ax6.grid(True)
    
    ax7 = plt.subplot(3,4,11)
    ax7.set_title('Derive 3')
    ax7.plot(y_x[3], derivee_y[3], "green", marker='x')
    ax7.grid(True)
    
    ax8 = plt.subplot(3,4,12)
    ax8.set_title('Derive 1 2 et 3')
    ax8.plot(y_x[1], derivee_y[1], "blue")
    ax8.plot(y_x[2], derivee_y[2], "gold")
    ax8.plot(y_x[3], derivee_y[3], "green")
    ax8.grid(True)
    
    if exp_num in [0,1,2,3,4,5,6,7,8,9]:
        plt.axvline(x=-0.4,color='gold', linestyle='--')
        plt.axvline(x=-2,color='gold', linestyle='--')
        plt.axvline(x=-0.8,color='green', linestyle='--')
        plt.axvline(x=-4,color='green', linestyle='--')
    
    if exp_num==10 : 
        plt.axvline(x=-0.15,color='gold', linestyle='--')
        plt.axvline(x=-0.28,color='gold', linestyle='--')
        plt.axvline(x=-0.3,color='green', linestyle='--')
        plt.axvline(x=-0.56,color='green', linestyle='--')
        
    # On fait le réglage des deux sets de deux barres
    # Le délai entre les deux barres gold correspond au délai attendu entre l'électrode 1 : bleue et la 2 : gold
    # Le délai entre les deux barres green correspond au délai attendu  entre l'électrode 1 : bleue  et la 3 : green
    # On devrait trouver un pic entre chacune de ces sets de 2 barres 
    
 
    if type == 'small' :
        ax4 = plt.subplot(248)
        ax4.set_title('1 and 4 (zoom)')
        ax4.plot(y_x[4], y[4], "purple", marker='o')
        ax4.grid(True)
     
    plt.suptitle('Correlation between two electrodes',fontsize=20)
    plt.show()

    return delay_list


def fourier_simple(y):                      # fourier transform; frequency analysis
    '''
    Plots the fourier transform of a given signal in frequency domain: 
        Parameters : 
            y(list): the chosen data from the experiment
        Returns :
            None
    '''
    fs = 24000
    y1 = np.array(y)    
    xf = fftpack.rfft(y1,2*len(y1))
    freqs = fftpack.rfftfreq(2*len(y1), d=1/fs)
    return [freqs,abs(xf)]


def fourier(y,tmin):                      # fourier transform; frequency analysis
    '''
    Plots the fourier transform of a given signal in frequency domain: 
        Parameters : 
            y(list): the chosen data from the experiment
        Returns :
            None
    '''
    fs = 24000
    y1 = np.array(y)
    # y1 += np.array([1/10*np.sin(2*np.pi*8000*x/fs) for x in range(y1.size)])          Test pour voir si la TdF marche bien (on attent un pic à 8000)

    xf = fftpack.rfft(y1,2*len(y1))
    freqs = fftpack.rfftfreq(2*y1.size, d=1/fs)

    plt.figure()
    plt.subplot(121)
    plt.title('Signal to analyse')
    plt.xlabel('Time (ms)')
    plt.grid(True)
    plt.ylabel('amplitude')
    plt.grid(True)
    plt.plot([tmin + k/(fs/1000) for k in range(y1.size)],y1)
    plt.subplot(122)
    plt.plot(freqs, abs(xf))
    plt.title('Fourier transform')
    plt.xlabel('Frequencies (Hz)')
    plt.xlim(0,3000)
    plt.grid(True)
    plt.ylabel('amplitude')
    plt.grid(True)
    plt.show()
    return [freqs, abs(xf)]

# f=lambda x: 1 if x%10<=1 and x%10>=-1 else 0              #Test pour faire marcher la TdF, faites pas gaffe
# f=lambda x: 1 if x<=1 and x>=-1 else 0
# f=lambda x: 1 if x==0 else 0

# fourier([sin(2*pi*10*x/100) for x in range(-100,100)])
# fourier([f(x/10) for x in range(-1000,1000)])
