from cProfile import label
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from signal_processing.processing.filters import *
from signal_processing.processing.spectral import *
from signal_processing.processing.trimming import *
from signal_processing.processing.correlation import *
import glob
import os
from scipy.interpolate import interp1d
from signal_processing.processing import *

DataMemory = {}

def load_header(headerpath):
    #Test : load_header(liste_graph[52])
    '''
    Loads header info in a pandaframe:
        Parameters:
            headerpath (str): path to the header file 
                Example :  "./Data/StimPetiteFibre1907\\11130SansStimSansFiltre\\00011130_header.txt"
        Returns:
            header (pd.dataframe): Dataframe containing info on all experiments 
    '''
    with open(headerpath, newline='') as f:
        lines = f.readlines()
        words = [line.strip("\r\n").split(',') for line in lines]
        column_title = words.pop(0)
        dic = {}
        for i, el in enumerate(column_title):
            dic[el] = [word[i] for word in words]
        df = pd.DataFrame(dic)
    return df


def load_path(path, legend="No info", shows=False):
    # Test : #load_path(liste_graph[0])
    '''
    Shows the plot related to the given path:
        Parameters:
            path (str): path to the file 
                path exemple :./Data/StimPetiteFibre1907\\11130SansStimSansFiltre\\00011130_11_01_1_0001_-10000.txt
            legend (str): txt to be shown under the plot, will contain description of the file
            shows (Boolean): Set to True so see plot
        Returns:
            shows plot if shows == True
            x,y : lists of value to be plotted

    '''
    with open(path, newline='') as f:
        lines = f.readlines()
        words = [line.strip("\r\n").split(',') for line in lines]
        words = [[int(value[0]), float(value[1])] for value in words]
        points = np.array(words)
        x = points[:, 0]
        y = points[:, 1]
        if shows == True:
            fig = plt.figure()
            fig.text(.5, .05, legend, ha='center')
            plt.plot(x, y)
            plt.show()
        return x, y


experience_table = [
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1127', '00011127_header.txt'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1128', '00011128_header.txt'),  
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1130SansStimSansFiltre', '00011130_header.txt'),
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1130StimSansFiltre', '00011130_header.txt'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1132SANSSTIMFILTRE', '00011132_header.txt'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1132STIMFILTRE', '00011132_header.txt'),  
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1133SANSSTIMFILTREREfCOMMUN', '00011133_header.txt'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience1', 'header.txt'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience2', '00011203_header.txt'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience3', '00011204_header.txt'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience4', '00011205_header.txt')]

path_table = [
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1127'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1128'),  
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1130SansStimSansFiltre'),
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1130StimSansFiltre'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1132SANSSTIMFILTRE'), 
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1132STIMFILTRE'),  
    os.path.join('.', 'Data', 'StimPetiteFibre1907', '1133SANSSTIMFILTREREfCOMMUN'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience1'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience2'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience3'),
    os.path.join('.', 'Data', 'ExperienceSurPlace', 'Experience4')]

liste_experience = glob.glob(os.path.join('.', 'Data', 'StimPetiteFibre1907', '**'))
liste_graph_big = glob.glob(os.path.join('.', 'Data', 'RecordsGrossesFibres1607', '**', '*.txt'))

# Aucune légende disponible pour ces expériences, il manque aussi une header pour Nouveau dossier(2)
# 3 expériences
experience_big_table = [
    os.path.join('.', 'Data', 'RecordsGrossesFibres1607', 'Nouveau dossier', '00011121_header.txt'),
    os.path.join('.', 'Data', 'RecordsGrossesFibres1607', 'Nouveau dossier (3)', '00011113_header.txt'),
    os.path.join('.', 'Data', 'RecordsGrossesFibres1607', 'Nouveau dossier (4)', '00011117_header.txt')]

experience_sur_place = ['D:/ALL/PA/Project/projet-douleur/Data/experiencesurplace/00011200_header.txt']

def load_exp(exp_num=0, 
                synchronisation=False,
                single_experiment=False,
                time_num=1,
                data_degradation=False,
                degradation_num=10,
                degradation_index=9,
                band=False,
                low=True,
                order=3,
                notch=False, 
                fmin=0,
                fmax=100,
                Q=1, 
                interpolation = False,
                shows=False,
                barre_a_delta=True,
                barre_b=False,
                barre_c=False,
                barre_grosse_fibre=False,
                trims=False,
                timeframe=[3,71], #[1.5,3] exp 10
                detrend=True,
                spectre=False,
                correlation=False,
                out=False,
                sg=False,
                Nfft=64):

    '''
    Shows the 4 channel-plot related to a given small fiber experiment, averaged amongst all distinct stimulation :
        Parameters:
            exp_num (int): number of the experiment, table is given in main.py for reference
            synchronisation (bool): Set to True to take into account the expected average delay for each fiber. Doesn't work well because of the the various possible delays 
            single_experiment (bool): Set to True to view a single experiement
            time_num (int) : number of the chosen single experiment (sorted by time)
            data_degradation (boolean) : If true and if single_experiment is False, process a degradation of data
            degradation_num (int) : decides the number of degradation
            degradation_index (int) : The index of the degree chosen to process, not greater than 'degradation_num-1'
            band (boolean) : If true, the resulting plot will be filtered with frequencies in [fmin,fmax] 
            low (bool) : If True, a low pass is applied with fmax as the maximum frequency
            order (int) : order of the bandpass filter
            notch (boolean) : If true, notch filter will be applied to remove frequency w0 with quality factor Q
            fmax (float): Any frequencies above fmax will be filtered by the chosen filter
            fmin (float): Any frequencies below fmin will be filtered by the chosen filter 
            shows (boolean) : Set to True so see plot
            trims (boolean) : Set to True to trim the plot of zeroes on the borders
            timeframe [int,int]: Trimming parameters in milisecond
            spectre (boolean) : Set to True to see the fourier transform of the signal (on the first channel)
            cor (boolean) : Set to True to see the correlation of the first 2 channels
            out (boolean) : Set to True to return x, data
            sg (boolean) : Set to True to print the time-frequency diagram
        Returns:
            shows plot if shows is True, also shows fourier transform if spectre is True and correlation of the first two channel if cor is True
            x, data lists of value to be plotted #NOT REALLY, NEEDS FIXING
        Note : 
            exp_num for small fibers is between 0 and 6 and for big fibers exp_num is either 0,1 or 2
    '''
    fs = 24000
    experience = experience_table
    path = path_table[exp_num]
    headerpath = experience[exp_num]
    header = load_header(headerpath)
    distinct_times = list(set(list(header.RecordingTime)))
    data = {1: [], 2: [], 3: [], 4: []}
    if single_experiment or data_degradation:
        distinct_times = list(set(list(header.RecordingTime)))
        distinct_times.sort()
        if single_experiment:
            chosen_time = [distinct_times[time_num]]
        else:
            step = int(len(distinct_times)/degradation_num)
            chosen_time = distinct_times[degradation_index*step:(degradation_index+1)*step]
        for time in chosen_time:
            header_small = header[header["RecordingTime"] == time]
            for index, row in header_small.iterrows():
                x, y = load_path(os.path.join(path, row["Filename"]))
                # ATTENTION Reste à rajouter le channel en legende
                if data[int(row['Channel'])] == []:
                    for i,el in enumerate(y):
                        data[int(row['Channel'])].append(el)
                else:
                    for i,el in enumerate(y):
                        if data[int(row['Channel'])][i]:
                            data[int(row['Channel'])][i] += el
        for key in data:
            data[key] = [item/len(chosen_time) for item in data[key]]
    else:
        for e in os.walk("./Data/Memory"): 
            l=e[2]       #l contient tous les elements de data/memory
        if not str(exp_num)+".csv" in l:
            for time in distinct_times:
                header_small = header[header["RecordingTime"] == time]
                for index, row in header_small.iterrows():
                    x, y = load_path(os.path.join(path, row["Filename"]))
                    # ATTENTION Reste à rajouter le channel en legende
                    if data[int(row['Channel'])] == []:
                        for i,el in enumerate(y):
                            data[int(row['Channel'])].append(el)
                    else:
                        for i,el in enumerate(y):
                            if data[int(row['Channel'])][i]:
                                data[int(row['Channel'])][i] += el
            with open("./Data/Memory/"+str(exp_num)+".csv","w") as f:
                for key in data.keys():
                    f.write("%s; %s\n" % (key, data[key]))
        else: 
            data={}
            with open("./Data/Memory/"+str(exp_num)+".csv","r") as f:      #on crée le fichier
                l=f.readlines()
                for i in range(4):
                    data[i+1]=[float(_) for _ in l[i][4:-2].split(', ')]
    x = np.array(range(1,len(data[1])+1))/24.000

    facteur=7 # facteur multplicatif du nombre de point apporté par l'inter

    x_inter = np.array(range(facteur,facteur*len(data[1])+1))/(facteur*24.000)

    if band:
        for key in data:
            data[key] = band_pass(data[key],order, fmin, fmax)  # Filtre passe bande

    if synchronisation :
        if exp_num==10 :
            decalage = 0.17
            # decalage attendu entre 0.15 et 0.28 ms
        else : 
            decalage = 0.4
            # decalage attendu entre 0.4 et 2 ms
        x = x[:-2*int(decalage*fs/1000)]
        # On garde des abscisses cohérentes avec la première électrode 
        data[1]=data[1][:-2*int(decalage*fs/1000)]
        data[2]=data[2][int(decalage*fs/1000):-int(decalage*fs/1000)]
        data[3]=data[3][2*int(decalage*fs/1000):] 
   
    
    if notch:
        for key in data:
            data[key] = notch(data[key], 50, Q, fmax)  # Filtre notch à 50 Hertz
     # for big fibers, it is still unsure if the sampling frequency is 24kHz
    
    if interpolation: 
        fig=plt.figure()
        interp={}
        for key in data:
            interp[key] = interp1d(x,data[key], kind='cubic')
        plt.plot(x, data[1], 'o', x_inter, interp[1](x_inter), '-')
        plt.title('Signal interpolé (éléctrode 1)')
        plt.show()
        for key in interp:
            interp[key]=[interp[key](x) for x in x_inter]

    if trims:
        x,data=trim(list(x),data,timeframe[0],timeframe[1])
        if interpolation:
            x_inter,interp=trim(list(x_inter),interp,timeframe[0],timeframe[1],fs=facteur*fs)
    
    if low:
        fourier_max=200
        fig, axs = plt.subplots(2, 2)
        if interpolation:
            four = fourier_simple(interp[1],fs=facteur*fs)
        else: 
            four= fourier_simple(data[1])
        for key,value in data.items():
            if key!= 4:
                axs[0, 0].plot(x, value)
                axs[0, 0].grid()
                axs[0, 1].plot(four[0][:fourier_max], four[1][:fourier_max],'tab:red',marker='x')
                axs[0, 1].vlines(fmax,min(four[1][:fourier_max]),max(four[1][:fourier_max]),color='black', linestyle='dashed')
                axs[0, 1].grid()
                data[key] = low_pass(value,order, fmax)
                if interpolation:
                    interp[key] = low_pass(interp[key],order,fmax,facteur*fs)
                axs[1, 0].plot(x, data[key])

        if interpolation:
            four = fourier_simple(interp[1],fs=fs*facteur)
        else: 
            four= fourier_simple(data[1])

        axs[1, 1].plot(four[0][:fourier_max], four[1][:fourier_max],'tab:red',marker='x')
        axs[1, 1].vlines(fmax,min(four[1][:fourier_max]),max(four[1][:fourier_max]),color='black', linestyle='dashed')
        axs[1, 1].set_title('Fourier après filtrage')
        axs[1, 1].set(xlabel='Frequence (Hz)', ylabel='Amplitude (mv)')
        axs[1, 1].grid()
        axs[0, 0].set_title('Signal Original')
        if single_experiment or data_degradation:
            if single_experiment:
                axs[0, 0].set(xlabel="exp_num: {d}; time_num: {s};".format(d=exp_num, s=time_num), ylabel='Amplitude (mv)')
            else:
                axs[0, 0].set(xlabel="exp_num: {d}; degradation_num: {s}; index: {t}".format(d=exp_num, s=degradation_num, t=degradation_index), ylabel='Amplitude (mv)')
        else :
            axs[0, 0].set(xlabel='Temps (ms)', ylabel='Amplitude (mv)')

    if detrend:
        for key in data:
            data[key]=detrender(data[key])
        
    '''if synchronisation :
        if exp_num==10 :
            decalage = 0.30
            # decalage attendu entre 0.15 et 0.28 ms
        else : 
            decalage = 0.4
            # decalage attendu entre 0.4 et 2 ms
        x = x[:-2*int(decalage*fs/1000)]
        # On garde des abscisses cohérentes avec la première électrode 
        data[1]=data[1][:-2*int(decalage*fs/1000)]
        data[2]=data[2][int(decalage*fs/1000):-int(decalage*fs/1000)]
        data[3]=data[3][2*int(decalage*fs/1000):]    '''

    if shows:
        fig = plt.figure()
        plt.xlabel("Time (ms)")
        plt.ylabel("Amplitude")
    for key in data:
        data[key] = [el/len(distinct_times) for el in data[key]]
        if shows:
            if key != 4 : 
                plt.plot(x, data[key], label='Channel '+str(key))


            
        '''if legend == "None":
                legend = "Stimulation intensity = "+header_small.loc[index, "StimIntensity"] + "   Recording Time= " + header_small.loc[index, "RecordingTimeText"] + \
                    "Sample Frequency = "+header_small.loc[index, "SampleFreq"] + " Modality = " + \
                    header_small.loc[index, "Modality"] + \
                    " Position = "+header_small.loc[index, "Position"]
        fig.text(.5, .05, legend, ha='center')'''
    
    # On place 2 barres qui correspondent à l'endroit où 
    # on s'attend à trouver la gaussienne de la première électrode connaissant la fenêtre de conduction typique des fibres concernées
    # La gaussienne doit commencer au niveau de la première barre et finir au niveau de la deuxième 

                    
    if shows:
        
        if barre_a_delta :
            if exp_num in [7,8]:
                plt.axvline(x=2,color='black', linestyle='--')
                plt.axvline(x=10,color='black', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0.3-0.5 ms between these electrodes')
            if exp_num in [0,1,2,3,4,5,6]:
                plt.axvline(x=5.2,color='black', linestyle='--')
                plt.axvline(x=26,color='black', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0.3-0.5 ms between these electrodes')
            if exp_num==9 : 
                plt.axvline(x=4,color='black', linestyle='--')
                plt.axvline(x=20,color='black', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0,15-0,28 ms between these electrodes')
        
        if barre_grosse_fibre :
            if exp_num==10 : 
                plt.axvline(x=1.5,color='black', linestyle='--')
                plt.axvline(x=2.8,color='black', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0.3-0.5 ms between these electrodes')
                
        if barre_b :
            if exp_num in [7,8]:
                plt.axvline(x=3.3,color='red', linestyle='--')
                plt.axvline(x=50,color='red', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0.3-0.5 ms between these electrodes')
            if exp_num in [0,1,2,3,4,5,6]:
                plt.axvline(x=8.7,color='red', linestyle='--')
                plt.axvline(x=130,color='red', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0.3-0.5 ms between these electrodes')
            if exp_num==9 : 
                plt.axvline(x=6.7,color='red', linestyle='--')
                plt.axvline(x=100,color='red', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0,15-0,28 ms between these electrodes')  
                
        if barre_c :
            if exp_num in [7,8]:
                plt.axvline(x=12.5,color='blue', linestyle='--')
                plt.axvline(x=125,color='blue', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0.3-0.5 ms between these electrodes')
            if exp_num in [0,1,2,3,4,5,6]:
                plt.axvline(x=32.5,color='blue', linestyle='--')
                plt.axvline(x=325,color='blue', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0.3-0.5 ms between these electrodes')
            if exp_num==9 : 
                plt.axvline(x=25,color='blue', linestyle='--')
                plt.axvline(x=250,color='blue', linestyle='--')
                #plt.text(6,35, 'we expect a delay of 0,15-0,28 ms between these electrodes')

        plt.xlabel('Time (ms)')   
        plt.ylabel('Amplitude (mV)')
        plt.legend()
        plt.grid()
        plt.show()  # affichage
    
    if spectre:
        if interpolation:
            fourier(interp[1],timeframe[0],fs=fs*facteur)
        else:
            fourier(data[1],timeframe[0])
    '''if cor:
        
        data[1]+=1/10*np.sin(2*np.pi*8000*np.array(x))'''# C'est quoi ca ?
    
    if spectre:
        fourier(data[1],timeframe[0])
        
    if correlation:
        correlation_all(data, exp_num) 
        
    if out:
        return x,data
    
    if sg:
        if interpolation:
            _,_,_,cb = plt.specgram(interp[1],Fs = facteur*fs, NFFT=Nfft, noverlap = Nfft/2, vmin = -120, vmax = 0)
        else:    
            _,_,_,cb = plt.specgram(data[1],Fs = fs, NFFT=Nfft, noverlap = Nfft/2, vmin = -120, vmax = 0)
        plt.ylim(top=1000)
        plt.xlabel("Time (s)", fontsize=20)
        plt.ylabel("Frequency (Hz)",fontsize=20)
        plt.colorbar(cb).set_label("Amplitude in DB",fontsize=20)
        if exp_num not in [10]:
            plt.plot([0.004,0.020,0.020,0.004,0.004],[1000,1000,0,0,1000], c="r")
        else:
            plt.plot([0.0015,0.0028,0.0028,0.0015,0.0015],[12300,12300,0,0,12300], c="r")
        plt.show()
    
    return 
    
