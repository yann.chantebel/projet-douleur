# Projet visant à quantifier la douleur

## Comment installer le projet 
```bash
git clone https://gitlab-student.centralesupelec.fr/yann.chantebel/projet-douleur.git       #cloner le repertoire gitlab en interne: 
pip install -r requirements.txt
```

## Ordres de grandeurs
|                              |         |
| ----------------------------------------------- | ----------------- |
| **First electrode at 5cm**                          ||
|                         Adelta signal position    | 2ms - 10ms        |
|                          Delay expected            | 0,4ms - (2ms)     |
|                                                    |                   |
|                          B position                | 3.3ms-50ms        |
|                          Delay expected            | 0.6ms-10ms        |
|                                                    |                   |
|                          C position                | 12.5ms-125ms      |
|                          Delay expected            | 2.5ms-25ms        |
|                              |         |
| **First electrode at 10cm**                         ||
|                          Adelta  signal position   | 4ms - 20ms        |
|                          Delay expected            | 0.4ms - (2ms)     |
|                                                    |                   |
|                          Big fiber signal position | 1,5ms - 2,8ms     |
|                          Delay expected            | 0,15ms - (0,28ms) |
|                                                    |                   |
|                          B position                | 6.7ms-100ms       |
|                          Delay expected            | 0.6ms-10ms        |
|                                                    |                   |
|                          C signal position         | 25ms-250ms        |
|                          Delay expected            | 2.5ms-25ms        |
|                              |         |
| **Velocity**                          ||
|                          Big fiber signal position | 35-65m/s          |
|             Adelta                    | 5-25m/s           |
|                          B                         | 1-15m/s           |
|                          C                         | 0.4-4m/s          |

## Comment lancer le projet 
Executer la commande : 
```python
main.py
```
Vous pouvez changer les paramètres de la fonction `load_exp()`:

Tous les paramètres:

```python
file_read.load_exp (exp_num=9,                                  # Choix du numéro de l'experience, voir [1]
                synchronisation=False,                          # Permet de synchroniser le signal sur les trois channels
                single_experiment=False,                        # Permet de visualiser un seul enregistrement et pas la moyenne
                time_num=1,                                     # Permet de choisir le numéro de l'unique enregistrement visualisé
                data_degradation=True,                          # Permet de moyenner les données par groupe de degradation_num
                degradation_num=10,                             
                degradation_index=9,                            
                band=False,                                     #True: filtre passe-bande, parametre: fmin, fmax, Q, order (du filtre)
                low=True,                                       #True: filtre passe-bas, parametre: fmax, order
                order=3,                                        #ordre des filtres /!\ ce nombre est multiplié par 2 pour supprimer la phase
                notch=False,                                    #filtre de notch, supprime la fréquence precise 50 Hz
                fmin=0,                                         #Paramètre de filtre
                fmax=100,                                       #Paramètre de filtre
                Q=1,                                            #Paramètre de filtre
                shows=True,                                     #Affiche les courbes
                barre_a_delta=True,                             #Affiche les ordres de grandeurs (barres verticales)
                barre_b=False,                                  #Affiche les ordres de grandeurs
                barre_c=False,                                  #Affiche les ordres de grandeurs   
                barre_grosse_fibre=False,                       #Affiche les ordres de grandeurs
                trims=True,                                     #Permet de couper le signal hors du timeframe definit après et notamment l'artefact
                timeframe=[3,71], # [1.5,30] exp 10             #Choisir selon les ordres de grandeurs du rapport
                detrend=True,                                   #Supprime la composante linéaire du signal à l'écran (signal remis a l'horizontal)
                spectre=False,                                  #Affiche le spectre du timeframe
                correlation=False,                              #Affiche la corrélation: autocorrélation et intercorrelation entre electrodes
                out=False,                                      #return x,data
                sg=False,                                       #Affiche le spectrogramme
                Nfft=64,                                        #Nombre de points de données temporelles pour lequel on fait une analyse spectrale
                interpolation = False)                          # Permet d'interpoler
```

[1]: On peut choisir parmis les 11 experiences suivantes:

condition = Type fibre, ecart entre premiere electrode et stimulation, temps de stimulation, fréquence d'echantillonage, Session d'experience

| Numéro Expérience | Description | Conditions |
|--- | --- | --- |
|0 | Sans Stim Sans Filtre   |                Petite fibre, 13cm, 300ms, 24000 Hz, 1ere |
|1 | 1127                       |             Petite fibre, 13cm, 50ms, 24000 Hz, 1ere|
|2 | 1128                       |             Petite fibre, 13cm, 300ms, 24000 Hz, 1ere|
|3 | Avec Stim Sans Filtre        |           Petite fibre, 13cm, 50ms, 24000 Hz, 1ere|
|4 | Sans Stim Avec Filtre        |           Petite fibre, 13cm, 300ms, 24000 Hz, 1ere|
|5 | Avec Stim Avec Filtre          |         Petite fibre, 13cm, 50ms, 24000 Hz, 1ere|
|6 | Sans Stimulation Filtrerefcommun  |      Petite fibre, 13cm, 300ms, 24000 Hz, 1ere|
|7 | Expérience sur place #1           |      Petite fibre, 5cm, 300ms, 24000 Hz, 2nd|
|8 | Expérience sur place #2             |    Petite fibre, 5cm, 300ms, 24000 Hz, 2nd|
|9 | Expérience sur place #3             |    Petite fibre, 10cm, 300ms, 24000 Hz, 2nd|
|10| Expérience sur place #4 (big fibers)  |  Grosse fibre, 10cm, 300ms, 24000 Hz, 2nd|






* Pour les grosses fibres nous vous conseillons 

```python 
file_read.load_exp (exp_num=10, 
                synchronisation=False,
                single_experiment=False,
                time_num=1,
                data_degradation=False,
                degradation_num=10,
                degradation_index=9,
                band=False,
                low=False, 
                order=3,
                notch=False, 
                fmin=0,
                fmax=100,
                Q=1, 
                shows=True,
                barre_a_delta=True,
                barre_b=False,
                barre_c=False,
                barre_grosse_fibre=True,
                trims=True,
                timeframe=[1.5,21], 
                detrend=True,
                spectre=True,
                correlation=True,
                out=False,
                sg=True,
                Nfft=64,
                interpolation = False)
```


* Pour observer le signal des petites fibres sans filtrage nous vous conseillons 

```python
file_read.load_exp (exp_num=9, 
                synchronisation=False,
                single_experiment=False,
                time_num=1,
                data_degradation=False,
                degradation_num=10,
                degradation_index=9,
                band=False,
                low=False, 
                order=3,
                notch=False, 
                fmin=0,
                fmax=100,
                Q=1, 
                shows=True,
                barre_a_delta=True,
                barre_b=False,
                barre_c=False,
                barre_grosse_fibre=True,
                trims=True,
                timeframe=[2.5,21], 
                detrend=True,
                spectre=True,
                correlation=True,
                out=False,
                sg=True,
                Nfft=64,
                interpolation = False)
```
* les paramètres de filtrage adaptés sont : 

L'ordre 3 est le plus adapté, on peut jouer sur `fmax` pour modifier le filtrage

```python
file_read.load_exp (exp_num=9, 
                synchronisation=False,
                single_experiment=False,
                time_num=1,
                data_degradation=False,
                degradation_num=10,
                degradation_index=9,
                band=False,
                low=True, 
                order=3,
                notch=False, 
                fmin=0,
                fmax=400,
                Q=1, 
                shows=True,
                barre_a_delta=True,
                barre_b=False,
                barre_c=False,
                barre_grosse_fibre=True,
                trims=True,
                timeframe=[2.5,21], 
                detrend=True,
                spectre=True,
                correlation=True,
                out=False,
                sg=True,
                Nfft=64,
                interpolation = False)
```
